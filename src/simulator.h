#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <vector>
#include "topo.h"
#include "switch.h"
#include "arrival_model.h"
#include "scheduling_algo.h"
#include "network.h"
#include <cstddef>
#include <iostream>

class Simulator{

private:
   static Network* d_networkPtr;
   static int d_pktNo;
   static int d_time;
   static std::vector<int> d_droppedPackets;
public:
   Simulator(std::vector<ArrivalModel*> arrivalModels, std::vector<SchedulingAlgo*> schedulingAlgos, Switch::SwitchType switchType, Topo* topoPtr, int maxBufferSize);
   ~Simulator();

   static int GetPacketNo();
   static Network* GetNetworkPtr();
   static int GetTime();
   static void UpdateTime();

   static void DropPacket(int packetNo);
   static void AdvanceTimeInstant();
   static std::vector<int> GetDroppedPackets();
};

#endif
