#include "switch.h"
#include <iostream>

Switch::Switch(){
}

Switch::~Switch(){
   delete d_schedulingAlgo;
   for (int portID = 1; portID <= d_numPorts; portID++){
         delete GetInputPortByID(portID);
         delete GetOutputPortByID(portID);
   }      
}

void 
Switch::SetNumPorts(int numPorts){
  d_numPorts = numPorts;
}

void
Switch::SetSchedulingAlgo(SchedulingAlgo* schedulingAlgo){
   d_schedulingAlgo = schedulingAlgo;
}

void
Switch::SetSwitchID(int switchID){
   d_switchID = switchID;
}

int
Switch::GetNumPorts(){
   return d_numPorts;
}

void
Switch::SetSwitchType(SwitchType switchType){
   d_switchType = switchType;
}

Switch::SwitchType
Switch::GetSwitchType(){
   return d_switchType;
}

void
Switch::AddInputPort(InputPort* inputPort){
   d_inputPorts.push_back(inputPort);
}

std::vector<InputPort*>
Switch::GetInputPorts(){
   return d_inputPorts;
}

InputPort*
Switch::GetInputPortByID(int inputPortID){
   for (int iterInputPort = 0; iterInputPort < d_inputPorts.size(); iterInputPort++){
      if (d_inputPorts[iterInputPort]->GetInputPortID() == inputPortID){
         return d_inputPorts[iterInputPort];
      }
   }
   return NULL;
}

void
Switch::AddOutputPort(OutputPort* outputPort){
   d_outputPorts.push_back(outputPort);
}

std::vector<OutputPort*>
Switch::GetOutputPorts(){
   return d_outputPorts;
}

OutputPort*
Switch::GetOutputPortByID(int outputPortID){
   for (int iterOutputPort = 0; iterOutputPort < d_outputPorts.size(); iterOutputPort++){
      if (d_outputPorts[iterOutputPort]->GetOutputPortID() == outputPortID){
         return d_outputPorts[iterOutputPort];
      }
   }
   return NULL;
}


SchedulingAlgo*
Switch::GetSchedulingAlgo(){
   return d_schedulingAlgo;
}
 
int
Switch::GetSwitchID(){
   return d_switchID;
}

void
Switch::AdvanceSwitch(){
   std::vector<InputPort*> inputPorts = GetInputPorts();
   for (int iterInputPort = 0; iterInputPort < inputPorts.size(); iterInputPort++){
       inputPorts[iterInputPort]->ReceivePacket();
   }
 
   ScheduleSwitch();

   std::vector<OutputPort*> outputPorts = GetOutputPorts();
   for (int iterOutputPort = 0; iterOutputPort < outputPorts.size(); iterOutputPort++){
       outputPorts[iterOutputPort]->SendPacket();
   } 
}

void
Switch::SetRandomPerm(){
   std::vector<int> randomPerm;
   for (int i=0; i < d_numPorts; i++) randomPerm.push_back(i);   
   std::random_shuffle ( randomPerm.begin(), randomPerm.end() );
   d_randomPerm.clear();
   for(int iter = 0; iter < randomPerm.size(); iter++){
      d_randomPerm.push_back(randomPerm[iter]);
   }
/*   for (int iter = 0; iter <  d_randomPerm.size(); iter++){
      std::cout << d_randomPerm[iter] << "  "<< std::endl;
   }*/
}

std::vector<int>
Switch::GetRandomPerm(){
 /*  for (int iter = 0; iter <  d_randomPerm.size(); iter++){
      std::cout << d_randomPerm[iter] << "  "<< std::endl;
   }*/
   return d_randomPerm;
}
