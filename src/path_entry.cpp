#include "path_entry.h"

PathEntry::PathEntry(){
}

PathEntry::~PathEntry(){
}

void
PathEntry::SetArrivalTime(int time){
  d_arrivalTime = time;
}

void
PathEntry::SetInputPort(InputPort* inputPort){
   d_inputPort = inputPort;
}

int
PathEntry::GetArrivalTime(){
   return d_arrivalTime;
}

InputPort* 
PathEntry::GetInputPort(){
   return d_inputPort;
}


