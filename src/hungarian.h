#ifndef HUNGARIAN_H
#define HUNGARIAN_H

#define INF 100000000    //just infinity
#include <iostream>
#include <vector>
#define MAX_SIZE 50
class Hungarian{

	public:
		Hungarian() ;
		~Hungarian();
   
		void InitLabels();
		int MaxWeight();
		void AddToTree(int x, int prevx); 
		void UpdateLabels();
		void Augment();  	
	        int Min(int, int );
	        int Max(int, int);
	        void SetValues(int, std::vector < std::vector<int> > );
		std::vector<int> ReturnY();
	private:	
		
		int cost [MAX_SIZE][MAX_SIZE] ;
		int n, max_match;        //n workers and n jobs
		int lx[MAX_SIZE], ly[MAX_SIZE];        //labels of X and Y parts
		int xy[MAX_SIZE];               //xy[x] - vertex that is matched with x,
		int yx[MAX_SIZE];               //yx[y] - vertex that is matched with y
		bool S[MAX_SIZE], T[MAX_SIZE];         //sets S and T in algorithm
		int slack[MAX_SIZE];            //as in the algorithm description
		int slackx[MAX_SIZE];           //slackx[y] such a vertex, that
                         // l(slackx[y]) + l(y) - w(slackx[y],y) = slack[y]
		int prev[MAX_SIZE];             //array for memorizing alternating paths
		

};


#endif



