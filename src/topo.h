#ifndef TOPO_H
#define TOPO_H

#include "switch.h"
class Network;

class Topo{

public:
   Topo();
   virtual ~Topo();
   enum TopoType{
      TOPO_SIMPLE};

   virtual Network* CreateNetwork(Switch::SwitchType switchType, int maxBufferSize) = 0;
   virtual int GetNumTrafficGenerators() = 0;
   virtual int GetNumTrafficSinks() = 0;
   virtual int GetNumSwitches() = 0;

   void SetTopoType(TopoType topoType);
   TopoType GetTopoType();
private:
   TopoType d_topoType;
};

#endif

