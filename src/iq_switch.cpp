#include "iq_switch.h"
#include "queued_input_port.h"
#include "non_queued_output_port.h"
#include "crossbar_config.h"
#include <iostream>
#include <cstdio>
#include <cstddef>

IQSwitch::IQSwitch(int numPorts, int maxBufferSize){
   SetSwitchType(Switch::IQ_SWITCH);
   SetNumPorts(numPorts);
   for (int portID = 1; portID <= numPorts; portID++){
       QueuedInputPort* newInputPort = new QueuedInputPort(maxBufferSize);
       newInputPort->SetInputPortID(portID);
       newInputPort->SetSwitch((Switch*) this);
       AddInputPort((InputPort*) newInputPort);

       NonQueuedOutputPort* newOutputPort = new NonQueuedOutputPort(); 
       newOutputPort->SetOutputPortID(portID);
       newOutputPort->SetSwitch((Switch*) this);
       AddOutputPort((OutputPort*) newOutputPort);
   }
   d_crossbarConfig = new CrossbarConfig(numPorts);
}

IQSwitch::~IQSwitch(){
   delete d_crossbarConfig;
}

CrossbarConfig*
IQSwitch::GetCrossbarConfig(){
   return d_crossbarConfig;
}

void
IQSwitch::SetCrossbarConfig(CrossbarConfig* crossbarConfig){
   d_crossbarConfig = crossbarConfig;
}

Packet*
IQSwitch::GetScheduledPacket(int outputPortID){
   int inputPortID = GetCrossbarConfig()->QueryCrossbar(outputPortID);
   if (inputPortID != 0){
      QueuedInputPort* inputPort = (QueuedInputPort*)GetInputPortByID(inputPortID);
      return inputPort->GetPacketForOutputPortID(outputPortID);
   }
   else
      return NULL;
}
  
void
IQSwitch::ScheduleSwitch(){
   SetRandomPerm();
   std::vector<SchedulingInput*> schedulingInputs;
   std::vector<InputPort*> inputPorts = GetInputPorts();
   std::vector<int> randomPerm = GetRandomPerm();
   for (int iterInputPort = 0; iterInputPort < inputPorts.size(); iterInputPort++){
      QueuedInputPort* currentInputPort = (QueuedInputPort*)(inputPorts[iterInputPort]);
      SchedulingInput* currentSchedulingInput = currentInputPort->GetSchedulingInput();
      currentSchedulingInput->MapSchedulingInput(randomPerm);
      schedulingInputs.push_back(currentSchedulingInput);
   } 
   std::vector<SchedulingInput*> shuffledSchedulingInputs(schedulingInputs.size(), NULL);
   
   for (int iterSchedInput = 0; iterSchedInput < schedulingInputs.size(); iterSchedInput++){
      shuffledSchedulingInputs[randomPerm[iterSchedInput]] = schedulingInputs[iterSchedInput];
   }

   GetSchedulingAlgo()->Schedule(d_crossbarConfig, shuffledSchedulingInputs);
   d_crossbarConfig->DemapCrossbar(randomPerm);
}
/*
bool
IQSwitch::CheckBackloggedPackets(){
   std::vector<InputPort*> inputPorts = GetInputPorts();
   bool backloggedPackets = false;
   for (int iterInputPort = 0; iterInputPort < inputPorts.size(); iterInputPort++){
      QueuedInputPort* currentInputPort = (QueuedInputPort*)(inputPorts[iterInputPort]);
      std::vector<Packet*> buffer = currentInputPort->GetBuffer();
      if(buffer.size() > 0){
          backloggedPackets = true;
          //std::cout<<"Input port "<<currentInputPort->GetInputPortID()<<" has "<<buffer.size()<< "packets"<<std::endl;
      }
   }
   return backloggedPackets;
} */
