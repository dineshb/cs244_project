Switch={'IQ','CIOQ', 'OQ'};
Algorithm={'FIFO','MSM','MWM','MLSM','PIM1','PIM2','PIM3','PIM4'}
traffic_model={'Asymmetric','Uniform'}
MaxBuffer=[1000; 2000 ;4000];
numInstants=[10000];
dir='cs244_project/src/results_Mar12_07/'

%%
for switch_iter=1:length(Switch)
    for algo_iter=1:length(Algorithm)
        for traffic_iter=1:length(traffic_model)
            for buf_iter=1:length(MaxBuffer)
                for  num_iter=1:length(numInstants)
                    
                    file_name=horzcat(dir,Algorithm{algo_iter},'_traffic_',num2str(traffic_model{traffic_iter})...
                        ,'_time_',num2str(numInstants(num_iter)),'_Switch_' , Switch{switch_iter},'_MaxBuffer_',num2str(MaxBuffer(buf_iter)),'.txt');
                    
                    if(exist(file_name,'file'))
                        A= load(file_name);
                        figure;
                        plot(A(:,1),A(:,2))
                        title_name=horzcat(Algorithm{algo_iter},' traffic ',num2str(traffic_model{traffic_iter})...
                            ,' time= ',num2str(numInstants(num_iter)),' Switch ' , Switch{switch_iter},' MaxBuffer ',num2str(MaxBuffer(buf_iter)),'.txt');
                        
                        title(title_name)
                    end
                    
                end
            end
        end
    end
end

