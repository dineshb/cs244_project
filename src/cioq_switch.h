#ifndef CIOQ_SWITCH_H
#define CIOQ_SWITCH_H

#include "crossbar_config.h"
#include "switch.h"

class CIOQSwitch:public Switch{

public:
   CIOQSwitch(int numPorts, int maxBufferSize, int speedup);
   ~CIOQSwitch();

   virtual void ScheduleSwitch();
   
   CrossbarConfig* GetCrossbarConfig();
   void SetCrossbarConfig(CrossbarConfig* crossbarConfig);
private:
   CrossbarConfig* d_crossbarConfig;
   int d_speedup;
};

#endif
