#include "non_queued_output_port.h"
#include "switch.h"
#include "packet.h"
#include "link.h"
#include "iq_switch.h"

NonQueuedOutputPort::NonQueuedOutputPort(){
   SetOutputPortType(NON_QUEUED_OUTPUT_PORT);
}

NonQueuedOutputPort::~NonQueuedOutputPort(){
}

void
NonQueuedOutputPort::SendPacket(){
  int outputPortID = GetOutputPortID();
  Packet* packet = NULL;
  if (GetSwitch()->GetSwitchType() == Switch::IQ_SWITCH){
     packet = ((IQSwitch*) GetSwitch())->GetScheduledPacket(outputPortID); 
  }
  if (packet != NULL){
     GetAttachedLink()->WritePacket(packet);
  }
  return;
}
