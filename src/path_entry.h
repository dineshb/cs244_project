#ifndef PATHENTRY_H
#define PATHENTRY_H

#include "input_port.h"

class PathEntry{

public:
   PathEntry();
   ~PathEntry();
 
   void SetArrivalTime(int time);
   void SetInputPort(InputPort* inputPort);
   
   int GetArrivalTime();
   InputPort* GetInputPort();
private:
   int d_arrivalTime;
   InputPort* d_inputPort;
};

#endif
