

#ifndef MWMSCHEDULING_H
#define MWMSCHEDULING_H

#include "scheduling_algo.h"
#include "scheduling_input.h"
#include "crossbar_config.h"

class MWMScheduling:public SchedulingAlgo{

public:
   MWMScheduling();
   ~MWMScheduling();
   std::vector<int> CountWeight(std::vector <int> temp );
   void MaxFlow(CrossbarConfig* crossbarConfig, std::vector< std::vector<int> > allOutput, std::vector <std::vector<int> > weight);
   virtual void Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs);
};

#endif

