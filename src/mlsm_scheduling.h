

#ifndef MLSMSCHEDULING_H
#define MLSMSCHEDULING_H

#include "scheduling_algo.h"
#include "scheduling_input.h"
#include "crossbar_config.h"

class MLSMScheduling:public SchedulingAlgo{

public:
   MLSMScheduling();
   ~MLSMScheduling();
   void Maximal(CrossbarConfig* crossbarConfig, std::vector< std::vector<int> > allOutput );
   virtual void Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs);
};

#endif

