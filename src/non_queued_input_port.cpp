#include "non_queued_input_port.h"
#include <cstddef>
#include "link.h"
#include "packet.h"
#include "output_port.h"
#include "queued_output_port.h"
#include "switch.h"

NonQueuedInputPort::NonQueuedInputPort(){
   SetInputPortType(NON_QUEUED_INPUT_PORT);
}

NonQueuedInputPort::~NonQueuedInputPort(){
}
    
void
NonQueuedInputPort::ReceivePacket(){
   Packet* packet = GetAttachedLink()->ReadPacket();
   if (packet != NULL){
        packet->UpdateHopCount();
        packet->SetNextOutputPortID();
        int nextOutputPortID = packet->GetNextOutputPortID();
        OutputPort* nextOutputPort = GetSwitch()->GetOutputPortByID(nextOutputPortID);
        ((QueuedOutputPort*) nextOutputPort)->AddPacketToBuffer(packet);
   }
}
