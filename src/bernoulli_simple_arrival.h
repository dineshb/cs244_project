#ifndef BERNOULLISIMPLEARRIVAL_H
#define BERNOULLISIMPLEARRIVAL_H

#include "arrival_model.h"

class BernoulliSimpleArrival:public ArrivalModel{
 
public:
   BernoulliSimpleArrival(double load);
   ~BernoulliSimpleArrival();
 
   virtual std::vector<int> GetDestinationIDs();
   void SetLoad(double load);

private:
   double d_load;
};

#endif   

