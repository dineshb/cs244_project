#ifndef NONQUEUEDOUTPUTPORT_H
#define NONQUEUEDOUTPUTPORT_H

#include "packet.h"
#include "output_port.h"

class NonQueuedOutputPort:public OutputPort{

public:
    NonQueuedOutputPort();
    virtual ~NonQueuedOutputPort();
   
    virtual void SendPacket();
};

#endif
