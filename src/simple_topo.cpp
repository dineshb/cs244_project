#include "topo.h"
#include "network.h"
#include "switch.h"
#include "simple_topo.h"
#include "iq_switch.h"
#include <iostream>
#include "oq_switch.h"
#include "cioq_switch.h"

SimpleTopo::SimpleTopo(int numPorts){
   SetTopoType(TOPO_SIMPLE);
   d_numPorts = numPorts;
}

SimpleTopo::~SimpleTopo(){
}

int
SimpleTopo::GetNumTrafficGenerators(){
   return d_numPorts;
}

int
SimpleTopo::GetNumTrafficSinks(){
   return d_numPorts;
}

int
SimpleTopo::GetNumSwitches(){
   return 1;
}

Network*
SimpleTopo::CreateNetwork(Switch::SwitchType switchType, int maxBufferSize){
  Network* network = new Network(GetTopoType());
  std::vector<Switch*> switches;
  std::vector<TrafficGenerator*> trafficGenerators;
  std::vector<TrafficSink*> trafficSinks;
  std::vector<Link*> links;

  Switch* newSwitch;
  if (switchType == Switch::IQ_SWITCH){
     newSwitch = new IQSwitch(d_numPorts, maxBufferSize);
     newSwitch->SetSwitchID(1);
  }
  if (switchType == Switch::OQ_SWITCH){
     newSwitch = new OQSwitch(d_numPorts, maxBufferSize);
     newSwitch->SetSwitchID(1);
  }
  if (switchType == Switch::CIOQ_SWITCH){
     int speedup = 2; 
     newSwitch = new CIOQSwitch(d_numPorts, maxBufferSize, speedup);
     newSwitch->SetSwitchID(1);
  }
  switches.push_back(newSwitch);

  std::vector<InputPort*> inputPorts = newSwitch->GetInputPorts();
  for(int iterInputPort = 0; iterInputPort < inputPorts.size(); iterInputPort++){
     TrafficGenerator* newTrafficGenerator = new TrafficGenerator();
     newTrafficGenerator->SetTrafficGeneratorID(iterInputPort + 1);

     Link* newLink = new Link((Source*) newTrafficGenerator, (Sink*) inputPorts[iterInputPort] );

     inputPorts[iterInputPort]->SetAttachedLink(newLink);
     newTrafficGenerator->SetAttachedLink(newLink);
     
     trafficGenerators.push_back(newTrafficGenerator);
     links.push_back(newLink);
  }

        
  std::vector<OutputPort*> outputPorts = newSwitch->GetOutputPorts();
  for(int iterOutputPort = 0; iterOutputPort < outputPorts.size(); iterOutputPort++){
     TrafficSink* newTrafficSink = new TrafficSink();
     newTrafficSink->SetTrafficSinkID(iterOutputPort + 1);

     Link* newLink = new Link((Source*) outputPorts[iterOutputPort], (Sink*) newTrafficSink );

     outputPorts[iterOutputPort]->SetAttachedLink(newLink);
     newTrafficSink->SetAttachedLink(newLink);
     
     trafficSinks.push_back(newTrafficSink);
     links.push_back(newLink);
  }
  
  network->AddSwitches(switches);
  network->AddTrafficGenerators(trafficGenerators);
  network->AddTrafficSinks(trafficSinks);
  network->AddLinks(links);
  return network;
}    


