#ifndef IQSWITCH_H
#define IQSWITCH_H

#include "input_port.h"
#include "output_port.h"
#include "crossbar_config.h"
#include "switch.h"

class IQSwitch:public Switch{

public:
    IQSwitch(int numPorts, int maxBufferSize);
    ~IQSwitch();
 
    Packet* GetScheduledPacket(int outputPortID);
    virtual void ScheduleSwitch();

    CrossbarConfig* GetCrossbarConfig();
    void SetCrossbarConfig(CrossbarConfig* crossbarConfig);
private:
    CrossbarConfig* d_crossbarConfig;
};

#endif
