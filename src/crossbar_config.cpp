#include "crossbar_config.h"


CrossbarConfig::CrossbarConfig(int numPorts){
  d_numPorts = numPorts;
  d_crossbarLinkages = std::vector<int> (d_numPorts, 0);
}

CrossbarConfig::~CrossbarConfig(){
}

int
CrossbarConfig::GetNumPorts(){
   return d_numPorts;
}

int 
CrossbarConfig::QueryCrossbar(int outputPortID){
   if (outputPortID > d_numPorts){
      return 0;
   }
   for (int inputPortID = 1; inputPortID <= d_numPorts; inputPortID++){
      if(d_crossbarLinkages[inputPortID - 1] == outputPortID){
         return inputPortID;
      }
   }
   return 0;
}
 
bool
CrossbarConfig::SetCrossbarLinkage(int inputPortID, int outputPortID){
    if(inputPortID > d_numPorts)
       return false;
    if(outputPortID > d_numPorts)
       return false;
    if((d_crossbarLinkages[inputPortID - 1] == 0)||(d_crossbarLinkages[inputPortID - 1] == outputPortID)){
       d_crossbarLinkages[inputPortID - 1] = outputPortID;
       return true;
    }
    else{
       return false;
    }
}

void 
CrossbarConfig::ResetCrossbar(){
   d_crossbarLinkages.clear();
   d_crossbarLinkages = std::vector<int> (d_numPorts, 0);   
}

std::vector<int>
CrossbarConfig::GetUnallocatedOutputPorts(){
   std::vector<int> unallocatedOutputPorts;
   std::vector<int>::iterator iterLinkage;
   for (int outputPortID = 1; outputPortID <= d_numPorts; outputPortID++){
       iterLinkage = std::find(d_crossbarLinkages.begin(), d_crossbarLinkages.end(), outputPortID);
       if( iterLinkage == d_crossbarLinkages.end()){
             // output port ID not found
             unallocatedOutputPorts.push_back(outputPortID);
       }
   }
   return unallocatedOutputPorts;
}


std::vector<int>
CrossbarConfig::GetUnallocatedInputPorts(){
   std::vector<int> unallocatedInputPorts;
   for (int inputPortID = 1; inputPortID <= d_numPorts; inputPortID++){
       if( d_crossbarLinkages[inputPortID - 1] == 0){
             // input port ID not allocated
             unallocatedInputPorts.push_back(inputPortID);
       }
   }
   return unallocatedInputPorts;
}

void
CrossbarConfig::DemapCrossbar(std::vector<int> randomPerm){
   std::vector<int> newCrossbarLinkages (d_numPorts, 0);
   for (int iterPort = 0; iterPort < d_numPorts; iterPort++){
      int mappedInputPort = iterPort;
      int actualInputPort = Demap(randomPerm, mappedInputPort);
      if (d_crossbarLinkages[iterPort] > 0){
         int mappedOutputPort = d_crossbarLinkages[iterPort] - 1;
         int actualOutputPort = Demap(randomPerm, mappedOutputPort);
         newCrossbarLinkages[actualInputPort] = actualOutputPort + 1;
      }
   }
   d_crossbarLinkages.clear();
   for (int iterLinkage = 0; iterLinkage < d_numPorts; iterLinkage++){
      d_crossbarLinkages.push_back(newCrossbarLinkages[iterLinkage]);
   } 
}  

int 
CrossbarConfig::Demap(std::vector<int> randomPerm, int mappedPort){
    int portActual;
    std::vector<int>::iterator it;
    it= std::find( randomPerm.begin(), randomPerm.end(), mappedPort);
    portActual= std::distance(randomPerm.begin(),it);
    return portActual; 
}
