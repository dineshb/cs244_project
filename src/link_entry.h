#ifndef LINKENTRY_H
#define LINKENTRY_H

#include "packet.h"

class LinkEntry{

public:
   LinkEntry(Packet* packet, int time);
   ~LinkEntry();
   
   int GetWriteTime();
   Packet* GetPacketOnLink();
private:
   Packet* d_packetOnLink;
   int d_writeTime;
};

#endif
