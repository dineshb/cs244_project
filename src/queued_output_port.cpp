#include "queued_output_port.h"
#include "simulator.h"
#include "link.h"

QueuedOutputPort::QueuedOutputPort(int maxBufferSize){
   SetOutputPortType(QUEUED_OUTPUT_PORT);
   d_maxBufferSize = maxBufferSize;
} 

QueuedOutputPort::~QueuedOutputPort(){
   for (int iterPacket = 0; iterPacket < d_buffer.size(); iterPacket++){
       delete d_buffer[iterPacket];
   }
}

std::vector<Packet*>
QueuedOutputPort::GetBuffer(){
   return d_buffer;
}

void
QueuedOutputPort::AddPacketToBuffer(Packet* packet){
   if (d_buffer.size() < d_maxBufferSize ){
      d_buffer.push_back(packet);
   }
   else {
      Simulator::DropPacket(packet->GetPacketNo());
      delete packet;
   }
}

void
QueuedOutputPort::SendPacket(){
   if(d_buffer.size() > 0){
      GetAttachedLink()->WritePacket(d_buffer[0]);
      d_buffer.erase(d_buffer.begin());
   }
}
      
