#ifndef PIMSCHEDULING_H
#define PIMSCHEDULING_H

#include "scheduling_algo.h"
#include "scheduling_input.h"
#include "crossbar_config.h"

class PIMScheduling:public SchedulingAlgo{

public:
   PIMScheduling(int iterations);
   ~PIMScheduling();
   void PimIteration(CrossbarConfig* crossbarConfig, std::vector< std::vector<int> > allDesiredOutputPortIDs);
   virtual void Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs);
private:
   int d_iterations;
};

#endif

