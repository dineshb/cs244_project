#ifndef TRAFFICGENERATOR_H
#define TRAFFICGENERATOR_H

#include "arrival_model.h"
#include "source.h"
#include <vector>

class TrafficGenerator:public Source{

public:
   TrafficGenerator();
   ~TrafficGenerator();

   int GetTrafficGeneratorID();
   ArrivalModel* GetArrivalModel();
   void SetArrivalModel(ArrivalModel* arrivalModel);
   void SetTrafficGeneratorID(int trafficGeneratorID);

   Packet* FormPacket(int trafficSinkID);
   virtual void SendPacket();
   void AdvanceTrafficGenerator();
private:
   int d_trafficGeneratorID;
   ArrivalModel* d_arrivalModel;
   std::vector<Packet*> d_buffer; 
};

#endif
