#include "bernoulli_complex_arrival.h"
#include "simulator.h"
#include "network.h"
#include "traffic_sink.h"
#include <cstdlib>

BernoulliComplexArrival::BernoulliComplexArrival(double load, std::vector<double> trafficMatrix){
   SetArrivalType(ARRIVAL_BERNOULLI_COMPLEX);
   d_load = load;
   d_trafficMatrix = trafficMatrix;
}

BernoulliComplexArrival::~BernoulliComplexArrival(){
}
 
std::vector<int>
BernoulliComplexArrival::GetDestinationIDs(){
    Network* network = Simulator::GetNetworkPtr();

    std::vector<int> destinationsForGeneratedPackets;
    for (int destinationID = 1; destinationID <= d_trafficMatrix.size(); destinationID++){
         float randomNum = ((double) rand())/((double) RAND_MAX);
         if (randomNum <= d_load*d_trafficMatrix[destinationID - 1])
             destinationsForGeneratedPackets.push_back(destinationID);
    }
          
    return destinationsForGeneratedPackets;
}

void
BernoulliComplexArrival::SetLoad(double load){
    d_load = load;
}

