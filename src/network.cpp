#include "network.h"
#include "topo.h"
Network::Network(Topo::TopoType topoType){
   d_topoType = topoType;
}
  
Network::~Network(){
  for (int iterSwitch = 0; iterSwitch < d_switches.size(); iterSwitch++){
     delete d_switches[iterSwitch];
  }
  for (int iterTrafficGen = 0; iterTrafficGen < d_trafficGenerators.size(); iterTrafficGen++){
     delete d_trafficGenerators[iterTrafficGen];
  }
  for (int iterTrafficSink = 0; iterTrafficSink < d_trafficSinks.size(); iterTrafficSink++){
     delete d_trafficSinks[iterTrafficSink];
  }
  for (int iterLink = 0; iterLink < d_links.size(); iterLink++){
     delete d_links[iterLink];
  }
}

void
Network::AddSwitches(std::vector<Switch*> switches){
  for (int iterSwitch = 0; iterSwitch < switches.size(); iterSwitch++){
     d_switches.push_back(switches[iterSwitch]);
  }
}

void
Network::AddLinks(std::vector<Link*> links){
  for (int iterLink = 0; iterLink < links.size(); iterLink++){
     d_links.push_back(links[iterLink]);
  }
}

void
Network::AddTrafficGenerators(std::vector<TrafficGenerator*> trafficGenerators){
  for (int iterTrafficGenerator = 0; iterTrafficGenerator < trafficGenerators.size(); iterTrafficGenerator++){
     d_trafficGenerators.push_back(trafficGenerators[iterTrafficGenerator]);
  }
}

void
Network::AddTrafficSinks(std::vector<TrafficSink*> trafficSinks){
  for (int iterTrafficSink = 0; iterTrafficSink < trafficSinks.size(); iterTrafficSink++){
     d_trafficSinks.push_back(trafficSinks[iterTrafficSink]);
  }
}

std::vector<Switch*>
Network::GetSwitches(){
  return d_switches;
}

Switch*
Network::GetSwitchByID(int switchID){
  for (int iterSwitch = 0; iterSwitch < d_switches.size(); iterSwitch++){
     if(d_switches[iterSwitch]->GetSwitchID() == switchID){
        return d_switches[iterSwitch];
     }
  }
  return NULL;
}

std::vector<TrafficGenerator*> 
Network::GetTrafficGenerators(){
  return d_trafficGenerators;
}

TrafficGenerator* 
Network::GetTrafficGeneratorByID(int trafficGeneratorID){
  for (int iterTrafficGen = 0; iterTrafficGen < d_trafficGenerators.size(); iterTrafficGen++){
     if(d_trafficGenerators[iterTrafficGen]->GetTrafficGeneratorID() == trafficGeneratorID){
        return d_trafficGenerators[iterTrafficGen];
     }
  }
  return NULL;
}

std::vector<TrafficSink*> 
Network::GetTrafficSinks(){
  return d_trafficSinks;
}

TrafficSink* 
Network::GetTrafficSinkByID(int trafficSinkID){
  for (int iterTrafficSink = 0; iterTrafficSink < d_trafficSinks.size(); iterTrafficSink++){
     if(d_trafficSinks[iterTrafficSink]->GetTrafficSinkID() == trafficSinkID){
        return d_trafficSinks[iterTrafficSink];
     }
  }
  return NULL;
}

std::vector<InputPort*>
Network::GetPath(TrafficGenerator* trafficGenerator, TrafficSink* trafficSink){
  std::vector<InputPort*> pathInputPorts;
  InputPort* firstInputPort = (InputPort*) trafficGenerator->GetAttachedLink()->GetSink();
  Switch* firstSwitch = firstInputPort->GetSwitch();
  OutputPort* lastOutputPort = (OutputPort*) trafficSink->GetAttachedLink()->GetSource();
  Switch* lastSwitch = lastOutputPort->GetSwitch();
  if (firstSwitch == lastSwitch){
     pathInputPorts.push_back(firstInputPort);
  }
  // Cover other cases later on !
  return pathInputPorts;
}

