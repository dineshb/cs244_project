#ifndef RUN_MLSM_H
#define RUN_MLSM_H

#include "simulator.h"
#include "switch.h"
#include "arrival_model.h"
#include "bernoulli_simple_arrival.h"
#include "bernoulli_complex_arrival.h"
#include "scheduling_algo.h"
#include "mlsm_scheduling.h"
#include "topo.h"
#include "simple_topo.h"
#include "queued_input_port.h"
#include "packet.h"
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <iostream>
#include <numeric>

void
run_mlsm(int numPorts, int maxBufferSize, int timeInstants, Switch::SwitchType switchType, std::vector<std::vector<double> >trafficMatrix){
  std::srand((unsigned) time(NULL));
 
  std::cout << "Running MLSM with numports = "<< numPorts<< " and maxBufferSize = "<< maxBufferSize << " for time = "<< timeInstants <<std::endl;
  if(switchType == Switch::IQ_SWITCH){
     std::cout <<" IQ Switch "<<std::endl;
  }
  else if (switchType == Switch::CIOQ_SWITCH){
     std::cout << " CIOQ Switch "<<std::endl;
  }
  // set topo
  SimpleTopo* topoPtr = new SimpleTopo(numPorts);


  for (double load = 0.1; load < 1; load = load + 0.1){
       // set sched algo
      std::vector<SchedulingAlgo*> schedulingAlgos;
      for (int iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
         MLSMScheduling* newSchedulingAlgo = new MLSMScheduling();
         schedulingAlgos.push_back(newSchedulingAlgo);
      }

      // set arrival models
      std::vector<ArrivalModel*> arrivalModels;
      for (int iterTrafficGen = 0; iterTrafficGen < topoPtr->GetNumTrafficGenerators(); iterTrafficGen++){
           BernoulliComplexArrival* newArrivalModel = new BernoulliComplexArrival(load, trafficMatrix[iterTrafficGen]);
           arrivalModels.push_back(newArrivalModel);
      }
  
      std::cout << "Running MLSM with numports = "<< numPorts<<" and load = "<< load << " and maxBufferSize = "<< maxBufferSize << " for time = "<< timeInstants <<std::endl;

      // start simulator
      Simulator* simulator = new Simulator(arrivalModels, schedulingAlgos, switchType, (Topo*) topoPtr, maxBufferSize);

       // run simulator
      for (int simTime = 0; simTime < timeInstants; simTime++){
         simulator->AdvanceTimeInstant();
      }

      // analyze average delay 
      Network* network = simulator->GetNetworkPtr();
      std::vector<TrafficSink*> trafficSinks = network->GetTrafficSinks();
     
      double cumulativeDelay = 0;
      int cumulativePackets = 0;
      for(int iterTrafficSink = 0; iterTrafficSink < trafficSinks.size(); iterTrafficSink++){
         TrafficSink* currTrafficSink = trafficSinks[iterTrafficSink];
         cumulativeDelay = cumulativeDelay + (currTrafficSink->GetTotalPackets())*(currTrafficSink->GetAverageDelay());
         cumulativePackets = cumulativePackets + currTrafficSink->GetTotalPackets();
      }
      int droppedPackets = (Simulator::GetDroppedPackets()).size();
      std::cout<<"Average delay encountered = "<< cumulativeDelay/cumulativePackets << " over "<<cumulativePackets <<" packets. Packets Dropped = "<< droppedPackets << std::endl;          
      delete simulator;
  }
}

#endif
