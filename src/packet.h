#ifndef PACKET_H
#define PACKET_H

#include "path_entry.h"
#include <vector>

class Packet{

public:
    Packet();
    ~Packet();

    void SetPath(std::vector<PathEntry*> pathEntries);
    void SetTrafficGeneratorID(int trafficGeneratorID);
    void SetTrafficSinkID(int trafficSinkID);
    void UpdateHopCount();

    std::vector<PathEntry*> GetPath();
    int GetPacketNo();
    int GetTrafficGeneratorID();
    int GetTrafficSinkID();
    int GetHopCount();
    int GetGenerationTime();
    int GetServiceTime();

    int GetNextOutputPortID();
    void SetNextOutputPortID();
private:
    int d_trafficGeneratorID;
    int d_generationTime;
    int d_trafficSinkID;
    int d_serviceTime;
    int d_hopCount;
    int d_packetNo;
    std::vector<PathEntry*> d_path;
   
    int d_nextOutputPortID;
};

#endif
