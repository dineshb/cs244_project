#ifndef NON_QUEUED_INPUT_PORT_H
#define NON_QUEUED_INPUT_PORT_H

#include "input_port.h"

class NonQueuedInputPort:public InputPort{

public:
    NonQueuedInputPort();
    ~NonQueuedInputPort();
    
    virtual void ReceivePacket();
};

#endif      

