#include "simulator.h"

Sink::Sink(){
}

Sink::~Sink(){
}

void 
Sink::SetSinkType(SinkType sinkType){
  d_sinkType = sinkType;
}

void
Sink::SetAttachedLink(Link* link){
  d_attachedLink = link;
}

Sink::SinkType
Sink::GetSinkType(){
  return d_sinkType;
}

Link*
Sink::GetAttachedLink(){
  return d_attachedLink;
}

