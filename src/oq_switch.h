#ifndef OQ_SWITCH_H
#define OQ_SWITCH_H

#include "switch.h"

class OQSwitch:public Switch{

public:
    OQSwitch(int numPorts, int maxBufferSize);
    ~OQSwitch();
    
    virtual void ScheduleSwitch();

};

#endif
