#ifndef SCHEDULINGALGO_H
#define SCHEDULINGALGO_H

#include "crossbar_config.h"
#include <vector>
#include "scheduling_input.h"

class SchedulingAlgo{

public:
   enum SchedulingAlgoType{
      FIFO, PIM, MWM, MSM , MLSM };
   SchedulingAlgo();
   virtual ~SchedulingAlgo();
   // each scheduling input comes from a particular input port and 
   // includes the desired output portIDs of the packets waiting there
   virtual void Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs) = 0;

   void SetSchedulingAlgoType(SchedulingAlgoType schedulingAlgoType);
private:
   SchedulingAlgoType d_schedulingAlgoType;
};

#endif
