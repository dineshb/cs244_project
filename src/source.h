#ifndef SOURCE_H
#define SOURCE_H

class Link;
class Packet;

class Source{

public:
   enum SourceType{
        TRAFFIC_GENERATOR,
        OUTPUT_PORT};
   Source();
   virtual ~Source();
 
   virtual void SendPacket() = 0;

   Link* GetAttachedLink();
   SourceType GetSourceType();
   void SetSourceType(SourceType sourceType);
   void SetAttachedLink(Link* link);  
private:
   SourceType d_sourceType;
   Link* d_attachedLink;
};

#endif
