import re
import itertools
import matplotlib as m
import os
if os.uname()[0] == "Darwin":
    m.use("MacOSX")
else:
    m.use("Agg")
import matplotlib.pyplot as plt
import argparse
import math

def read_list(fname, delim=' '):
    lines = open(fname).xreadlines()
    ret = []
    for l in lines:
        ls = l.strip().split(delim)
        ret.append(ls)
    return ret


def col(n, obj = None, clean = lambda e: e):
    """A versatile column extractor.

    col(n, [1,2,3]) => returns the nth value in the list
    col(n, [ [...], [...], ... ] => returns the nth column in this matrix
    col('blah', { ... }) => returns the blah-th value in the dict
    col(n) => partial function, useful in maps
    """
    if obj == None:
        def f(item):
            return clean(item[n])
        return f
    if type(obj) == type([]):
        if len(obj) > 0 and (type(obj[0]) == type([]) or type(obj[0]) == type({})):
            return map(col(n, clean=clean), obj)
    if type(obj) == type([]) or type(obj) == type({}):
        try:
            return clean(obj[n])
        except:
            print T.colored('col(...): column "%s" not found!' % (n), 'red')
            return None
    # We wouldn't know what to do here, so just return None
    print T.colored('col(...): column "%s" not found!' % (n), 'red')
    return None


def figure_args(args, algo , maxBuffer, switch, traffic, time  ):
    color = ['g', 'b', 'k', 'r', 'y', 'c', 'r', 'r']
    
    i = 0;
    for algo_type in algo:
        data = read_list(args.directory+"/"+algo_type+"_traffic_"+traffic+"_time_"+time+"_Switch_"+switch[i]+"_MaxBuffer_"+maxBuffer+".txt")
        load = map(float, col(0, data))
        delay = map(float, col(1, data))
        packets = map(float, col(2, data))
        packets_dropped = map(float, col(3, data))
        for x in range(len(packets_dropped)):
	     delay[x]=packets_dropped[x]/(packets_dropped[x]+ packets[x])
	if(switch[i]=="OQ"):
            plt.plot(load, delay,'-o', label = switch[i], color = color[i])
	else:
            plt.plot(load, delay,'-o', label = algo_type+" "+switch[i], color = color[i])
        i = i +1 
        
    
    plt.title(traffic+" traffic, over "+time+" time instants with max buffer size "+maxBuffer)
    plt.ylabel("Ratio of packets Dropped")
    plt.xlabel("Load")
    plt.xlim(0, 1)
    plt.ylim(0, 1 )
    plt.legend(loc = 2)
    plt.savefig("plots/Uniform_PAPER_dropped_packet_MaxBuffer_"+maxBuffer+"_Time_"+time+"_"+traffic+".png")

def main():
	
    parser = argparse.ArgumentParser()
    parser.add_argument('--directory', '-d',
                 help="Directory of txt files",
                 required=True,
                 action="store",
                 dest="directory")
    
    args = parser.parse_args()
    
    algo      = ["MLSM","PIM4","PIM3","PIM2", "PIM1", "FIFO"]
    maxBuffer = "2000"
    switch =   [ "OQ" , "IQ",  "IQ",   "IQ"   ,"IQ",   "IQ" ]
    traffic = "Uniform"
    time = "50000"
    figure_args(args, algo , maxBuffer, switch, traffic, time  )

	
	
	
	
if __name__ == "__main__":
     main()

