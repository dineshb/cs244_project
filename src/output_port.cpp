#include "output_port.h"

OutputPort::OutputPort(){
   SetSourceType(OUTPUT_PORT);
}

OutputPort::~OutputPort(){
}

void
OutputPort::SetOutputPortType(OutputPortType outputPortType){
   d_outputPortType = outputPortType;
}

void
OutputPort::SetOutputPortID(int outputPortID){
   d_outputPortID = outputPortID;
}

void
OutputPort::SetSwitch(Switch* attachedSwitch){
   d_switch = attachedSwitch;
}

int
OutputPort::GetOutputPortID(){
   return d_outputPortID;
}

Switch*
OutputPort::GetSwitch(){
   return d_switch;
}

OutputPort::OutputPortType
OutputPort::GetOutputPortType(){
   return d_outputPortType;
}
