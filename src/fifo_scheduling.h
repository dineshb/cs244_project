#ifndef FIFOSCHEDULING_H
#define FIFOSCHEDULING_H

#include "scheduling_algo.h"
#include "scheduling_input.h"
#include "crossbar_config.h"

class FIFOScheduling:public SchedulingAlgo{

public:
   FIFOScheduling();
   ~FIFOScheduling();
   virtual void Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs);
};

#endif

