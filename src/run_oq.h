#ifndef RUN_OQ_H
#define RUN_OQ_H

#include "simulator.h"
#include "switch.h"
#include "arrival_model.h"
#include "bernoulli_simple_arrival.h"
#include "scheduling_algo.h"
#include "fifo_scheduling.h"
#include "topo.h"
#include "simple_topo.h"
#include "queued_input_port.h"
#include "packet.h"
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <iostream>
#include <numeric>

void
run_oq(int numPorts, int maxBufferSize, int timeInstants){
  std::srand((unsigned) time(NULL));

  std::cout << "Running OQ with numports = "<< numPorts<< " and maxBufferSize = "<< maxBufferSize << " for time = "<< timeInstants <<std::endl;
  // set topo
  SimpleTopo* topoPtr = new SimpleTopo(numPorts);

  // set switch type
  Switch::SwitchType switchType = Switch::OQ_SWITCH;


  for (double load = 0.1; load <= 1; load = load + 0.1){
       // set sched algo - Doesnt matter !!
      std::vector<SchedulingAlgo*> schedulingAlgos;
      for (int iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
         FIFOScheduling* newSchedulingAlgo = new FIFOScheduling();
         schedulingAlgos.push_back(newSchedulingAlgo);
      }

      // set arrival models
      std::vector<ArrivalModel*> arrivalModels;
      for (int iterTrafficGen = 0; iterTrafficGen < topoPtr->GetNumTrafficGenerators(); iterTrafficGen++){
           BernoulliSimpleArrival* newArrivalModel = new BernoulliSimpleArrival(load);
           arrivalModels.push_back(newArrivalModel);
      }
  
      std::cout << "Running OQ with numports = "<< numPorts<<" and load = "<< load << " and maxBufferSize = "<< maxBufferSize << " for time = "<< timeInstants <<std::endl;

      // start simulator
      Simulator* simulator = new Simulator(arrivalModels, schedulingAlgos, switchType, (Topo*) topoPtr, maxBufferSize);

       // run simulator
      for (int simTime = 0; simTime < timeInstants; simTime++){
         simulator->AdvanceTimeInstant();
      }

      // analyze average delay 
      Network* network = simulator->GetNetworkPtr();
      std::vector<TrafficSink*> trafficSinks = network->GetTrafficSinks();
     
      double cumulativeDelay = 0;
      int cumulativePackets = 0;
      for(int iterTrafficSink = 0; iterTrafficSink < trafficSinks.size(); iterTrafficSink++){
         TrafficSink* currTrafficSink = trafficSinks[iterTrafficSink];
         cumulativeDelay = cumulativeDelay + (currTrafficSink->GetTotalPackets())*(currTrafficSink->GetAverageDelay());
         cumulativePackets = cumulativePackets + currTrafficSink->GetTotalPackets();
      }
      int droppedPackets = (Simulator::GetDroppedPackets()).size();
      std::cout<<"Average delay encountered = "<< cumulativeDelay/cumulativePackets << " over "<<cumulativePackets <<" packets. Packets Dropped = "<< droppedPackets << std::endl;          
      delete simulator;
  }
}

#endif
