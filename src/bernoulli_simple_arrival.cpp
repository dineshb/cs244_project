#include "bernoulli_simple_arrival.h"
#include "simulator.h"
#include "network.h"
#include "traffic_sink.h"
#include <cstdlib>

BernoulliSimpleArrival::BernoulliSimpleArrival(double load){
   SetArrivalType(ARRIVAL_BERNOULLI_SIMPLE);
   d_load = load;
}

BernoulliSimpleArrival::~BernoulliSimpleArrival(){
}
 
std::vector<int>
BernoulliSimpleArrival::GetDestinationIDs(){
    Network* network = Simulator::GetNetworkPtr();
    int numTrafficSinks = (network->GetTrafficSinks()).size();

    std::vector<int> destinationsForGeneratedPackets;
    for (int destinationID = 1; destinationID <= numTrafficSinks; destinationID++){
         float randomNum = ((double) rand())/((double) RAND_MAX);
         if (randomNum <= d_load/numTrafficSinks)
             destinationsForGeneratedPackets.push_back(destinationID);
    }
          
    return destinationsForGeneratedPackets;
}

void
BernoulliSimpleArrival::SetLoad(double load){
    d_load = load;
}

