#ifndef CROSSBARCONFIG_H
#define CROSSBARCONFIG_H

#include <vector>
#include <algorithm>

class CrossbarConfig{

public:
    CrossbarConfig(int numPorts);
    ~CrossbarConfig();
    
    int GetNumPorts();
    
    //returns 0 if the output port is not connected to anything
    int QueryCrossbar(int outputPortID);
    //returns true if the set was successful and false otherwise
    bool SetCrossbarLinkage(int inputPortID, int outputPortID);
    void ResetCrossbar();

    std::vector<int> GetUnallocatedOutputPorts();
    std::vector<int> GetUnallocatedInputPorts();
    
    void DemapCrossbar(std::vector<int> randomPerm);
    int Demap(std::vector<int> randomPerm, int mappedPortID);
private:
    // d_crossbarLinkages[i] is the outputPortID connected to input port i
    std::vector<int> d_crossbarLinkages;
    int d_numPorts;
};

#endif
