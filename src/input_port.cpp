#include "simulator.h"

InputPort::InputPort(){
  SetSinkType(Sink::INPUT_PORT);
}

InputPort::~InputPort(){
}

void
InputPort::SetSwitch(Switch* attachedSwitch){
   d_switch = attachedSwitch;
}

void
InputPort::SetInputPortID(int inputPortID){
  d_inputPortID = inputPortID;
}

void
InputPort::SetInputPortType(InputPortType inputPortType){
  d_inputPortType = inputPortType;
}

Switch*
InputPort::GetSwitch(){
  return d_switch;
}

int
InputPort::GetInputPortID(){
   return d_inputPortID;
}

InputPort::InputPortType
InputPort::GetInputPortType(){
   return d_inputPortType;
}


   
